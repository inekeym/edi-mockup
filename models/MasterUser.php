<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "master_user".
 *
 * @property int $id
 * @property string $username
 * @property string $password
 * @property string $fullname
 * @property string $alamat
 * @property string $email
 * @property string $telepon
 * @property int $role
 * @property string $created_date
 * @property int $flag 0. Not Active 1. Active
 */
class MasterUser extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    public $authKey;
    public $accessToken;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'master_user';
    }

  
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username','password'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'password' => 'Password',
        ];
    }

    public function getAuthKey(){
        return $this->authKey;
    }

    public function getId(){
        return $this->id;
    }
    public function getRole(){
        return $this->role;
    }
    public function validateAuthKey($authKey){
        return $this->authKey === $authKey;
    }
    

    public static function findIdentity($id){
        return self::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null){
        throw new \yii\base\NotSupportedException();
    }

    public static function findByUsername($username){
        return self::findOne(['username' => $username]);
    }

    public function validatePassword($password){
        return $this->password === $password;
    }
    public function getFlagText()
    {
        if($this->flag == 0)
            return 'Non-Active';
        else {
            return 'Active';
        }
    }
    public function getRoleText()
    {
        if($this->role == 0)
            return 'Super Admin';
        else if($this->role == 1)
            return 'Operational';
        else if($this->role == 2)
            return 'Finance';    
        else if($this->role == 3)
            return 'Vendor';
        else {
            return 'Approver';
        }
    }
}
