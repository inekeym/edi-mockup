<?php

namespace app\models;

use Yii;

class Pelamar extends \yii\db\ActiveRecord
{
    // public $pendidikan_terakhir;
    // public $riwayat_pelatihan;
    // public $riwayat_pekerjaan;
    
    public function rules() {
      return [       
        [['posisi','created_by','nama','no_ktp','tempat_lahir','tgl_lahir','jenis_kelamin','agama','gol_dar','status','alamat_ktp','orang_terdekat','skill','bersedia_seluruh_indo', 'penghasilan_perbulan'],'required'],
        [['alamat_skg','email','no_tlp',],'safe'],
        [['no_ktp'], 'string', 'max' => 16],
        ['email', 'email', 'message' => 'Please enter a valid email address.'],
        ['email', 'validateEmailWithAtSymbol'],

       ];
     
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pelamar';
    }
  
    /**
     * {@inheritdoc}
     */
 
    /**
     * {@inheritdoc}
     */
 
   
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->tgl_lahir = Yii::$app->formatter->asDate($this->tgl_lahir, 'yyyy-MM-dd');
            foreach ($this->attributes as $attribute => $value) {
                if (is_string($value)) {
                    $this->$attribute = strtoupper($value);
                }
            }

            return true;
        }

        return false;
    }
    public function validateEmailWithAtSymbol($attribute, $params)
    {
        if (strpos($this->$attribute, '@') === false) {
            $this->addError($attribute, 'The email address must contain the @ symbol.');
        }
    }
    public function getCity()
    {
        return $this->hasOne(MasterCity::className(), ['id' => 'tempat_lahir']);
    }
    public function getPendidikan()
    {
        return $this->hasMany(PendidikanTerakhir::className(), ['id_pelamar' => 'id']);
    }
    public function getPekerjaan()
    {
        return $this->hasMany(RiwayatPekerjaan::className(), ['id_pelamar' => 'id']);
    }
    public function getPelatihan()
    {
        return $this->hasMany(RiwayatPelatihan::className(), ['id_pelamar' => 'id']);
    }
    public static function selectionAgama(){
        $getSelection = [
            0 => "ISLAM",
            1 => "PROTESTAN",
            2 => "KATOLIK",
            3 => "BUDHA",
            4 => "HINDU",
            5 => "KONGHUCU"
        ]; 
        
        return $getSelection;
    }
    public static function selectionGoldar(){
        $getSelection = [
            0 => "O",
            1 => "A",
            2 => "B",
            3 => "AB",
        ]; 
        
        return $getSelection;
    }
    public static function selectionStatus(){
        $getSelection = [
            0 => "LAJANG",
            1 => "KAWIN",
            2 => "CERAI HIDUP",
            3 => "CERAI MATI",
        ]; 
        
        return $getSelection;
    }
    public static function selectionPendidikan(){
        $getSelection = [
            1 => 'SD',
            2 => 'SMP',
            3 => 'SMA', 
            4 => 'S1',
            5 => 'S2',
            6 => 'S3'
        ];
        return $getSelection;
    }
    public function deleteRelatedRecords()
    {
        PendidikanTerakhir::deleteAll(['id_pelamar' => $this->id]);
        RiwayatPelatihan::deleteAll(['id_pelamar' => $this->id]);
        RiwayatPekerjaan::deleteAll(['id_pelamar' => $this->id]);
    }
}
