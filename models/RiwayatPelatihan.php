<?php

namespace app\models;

use Yii;

class RiwayatPelatihan extends \yii\db\ActiveRecord
{
    public function rules() {
      return [       
        [['id_pelamar','nama_kursus','sertifikat','tahun_awal','tahun_akhir'],'required'],
       ];
     
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'riwayat_pelatian';
    }
  
    /**
     * {@inheritdoc}
     */
 
    /**
     * {@inheritdoc}
     */
   
    public function getPelamar()
    {
        return $this->hasOne(Pelamar::className(), ['id' => 'id_pelamar']);
    }
    
}
