<?php

namespace app\models;

use Yii;

class PendidikanTerakhir extends \yii\db\ActiveRecord
{
    public function rules() {
      return [       
        [['id_pelamar','jenjang','nama_institusi','tahun_lulus'],'required'],
        [['ipk','jurusan'],'safe']
       ];
     
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pendidikan_terakhir';
    }
  
    /**
     * {@inheritdoc}
     */
 
    /**
     * {@inheritdoc}
     */
   
    public function getPelamar()
    {
        return $this->hasOne(Pelamar::className(), ['id' => 'id_pelamar']);
    }
    
}
