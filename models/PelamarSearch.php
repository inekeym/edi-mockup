<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Pelamar;

/**
 * CompaniesSearch represents the model behind the search form about `backend\models\Companies`.
 */
class PelamarSearch extends Pelamar
{
   
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama','posisi','tempat_lahir'],'safe'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $user = Yii::$app->user->identity->id;
        // $design = (new \yii\db\Query())
        //     ->select('id_ps')
        //     ->from('designer_job')
        //     ->all();
        // $query = MasterProductionSheet::find()->joinWith('designerjob')->where(['production_sheet.flag' => 1])->andWhere(['not in','id',$design]);
        $query = Pelamar::find();

                
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [ 'pageSize' => 10 ],
        ]);
    
        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $query->andFilterWhere([
            'tempat_lahir' => $this->tempat_lahir,
        ]);
        $query->andFilterWhere(['like', 'nama', $this->nama]);
        // ->andFilterWhere(['like', 'total_gross_comm', $this->total_gross_comm])
        // ->andFilterWhere(['like', 'total_spending_iklan_kum', $this->total_spending_iklan_kum])
        // ->andFilterWhere(['like', 'nominal_wfh', $this->nominal_wfh]);
     
        return $dataProvider;
    }
}