<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MasterUser;

/**
 * CompaniesSearch represents the model behind the search form about `backend\models\Companies`.
 */
class UserSearch extends MasterUser
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // [['username','fullname','email','address','phone','role','flag'],'safe'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = MasterUser::find()->where(['flag'=>1]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            // 'sort' => ['attributes' => ['status', 'created_date']]
        ]);

        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // $query->andFilterWhere([
        //     'fullname' => $this->fullname,
        //     'username' => $this->username,
        //     'email' => $this->email,
        //     'address' => $this->address,
        //     'phone' => $this->phone,
        //     'role' => $this->role,
        //     'flag' => $this->flag,

        // ]);
    
        // $query->orFilterWhere(['like', 'fullname', $this->fullname])
        //         ->orFilterWhere(['like', 'username', $this->username])
        //         ->orFilterWhere(['like', 'email', $this->email])
        //         ->orFilterWhere(['like', 'address', $this->address])
        //         ->orFilterWhere(['like', 'phone', $this->phone])
        //         ->orFilterWhere(['like', 'flag', $this->flag])
        //         ->orFilterWhere(['like', 'role', $this->role]);
    
        return $dataProvider;
    }
}