<?php

namespace app\components;
use Yii;

class BaseController extends \yii\web\Controller
{
    public function init()
    {
        parent::init();
    }

    public function behaviors() 
    {
        return [
            'access' => [
                'class' =>  \yii\filters\AccessControl::class,
                'rules' => [
                        [
                            'actions' => ['login', 'error'],
                            'allow' => true,
                        ],
                        [
                            'allow' => true,
                            'roles' => ['@'],
                        ],
                        [
                            'allow' => false,
                            'roles' => ['?'],
                        ],
                    ],
                ]
        ];
     }      

    // public function beforeAction($action){
    //     var_dump($this->behaviors());
        
        // echo 'a';exit;
        // echo \Yii::$app->request->url;
        // var_dump(Yii::$app->user->loginUrl);
        // exit;
        // if (parent::beforeAction($action)){
        //     if (Yii::$app->user->isGuest){
        //         Yii::$app->user->loginUrl = ['site/login', 'return' => \Yii::$app->request->url];
        //         return $this->redirect(Yii::$app->user->loginUrl)->send();
        //     }
        // }
        // echo 'a';
    // }
}