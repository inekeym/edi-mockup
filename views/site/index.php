<?php

/* @var $this yii\web\View */

$this->title = 'EDI Mockup Dashboard';
?>


<div class="site-index">

    <div class="jumbotron">
        <?php
          $bytes = disk_free_space(".");
          $si_prefix = array( 'B', 'KB', 'MB', 'GB', 'TB', 'EB', 'ZB', 'YB' );
          $base = 1024;
          $class = min((int)log($bytes , $base) , count($si_prefix) - 1);
          $disk_usage = sprintf('%1.2f' , $bytes / pow($base,$class)) . ' ' . $si_prefix[$class];
        ?>

        <h2>Welcome,<?= Yii::$app->user->identity->username ?>!</h2>
    </div>

    <div class="body-content">

        <div class="row">
        </div>

    </div>
</div>
