<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

$this->title = "Master Data User";
?>
<div class="row">
    <div class="col-md-12">
        <h3>Master User</h3>
        <hr/>
       
    </div>
</div>

<?php $form = ActiveForm::begin(); ?>
 
    <?= $form->field($model, 'username')->input('username', ['placeholder' => "Masukan username "])?>
    <?= $form->field($model, 'password')->passwordInput(['required'=>($model->id ? false: true)])?>
    <?= $form->field($model, 'fullname')->input('fullname', ['placeholder' => "Masukan nama lengkap"]) ?>
    <?= $form->field($model, 'email')->input('email', ['placeholder' => "Masukan email "]) ?>
    <?= $form->field($model, 'address')->input('address', ['placeholder' => "Masukan alamat "]) ?>
	<?= $form->field($model, 'phone')->input('phone', ['placeholder' => "Masukan nomor telepon "]) ?>
    <?= $form->field($model, 'role')->dropDownList(['0'=>'Super Admin']); ?>


    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']); ?>
 
<?php ActiveForm::end(); ?>