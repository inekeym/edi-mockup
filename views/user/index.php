<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;
use yii\web\YiiAsset;
use yii\widgets\LinkPager;
use kartik\grid\GridView;

YiiAsset::register($this);

$this->title = "User";
?>

<div class="row">
    <div class="col-md-12">
        <h3> User</h3>
        <hr/>
       
    </div>
</div>

    
<div>   
    <div class="row">
    <div class="pull-right">
            <div class="col-md-4 pull-right">
            <!-- <?= Html::a('Add New', ['user/create'], ['class' => 'btn btn-success pull-right']); ?> <br> -->
            </div>
        </div>
    </div>
    <br>
    <?php
       
        $gridColumns = [
            ['class' => 'kartik\grid\SerialColumn',
            'width'=>'5%'
            ],
            [
                'attribute' => 'fullname', //nama field-1
                'vAlign' => 'middle',
            ],
    
            ['attribute'=> 'username',
            'vAlign' => 'middle',            //nama field-2
              'format'=>'raw',
              'label'=> "Username",
            ],
    
            [
              'attribute' => 'email',
              'vAlign' => 'middle',              //nama field-3
              'label' => 'Email',
            ],
            [
                'attribute' => 'address', //nama field-4
                'label' => 'Address', //nama field-4
                'vAlign' => 'middle',
            ],
            [
                'attribute' => 'phone', //nama field-5
                'label' => 'Phone No', //nama field-4
                'vAlign' => 'middle',

            ],
            [
                'vAlign' => 'middle',
                'attribute' => 'role',
                'value' => function($model){
                        return $model->roleText;
                },
                'filter'=>array("0"=>"Super Admin"),
                'label'=> "Hak Akses",

            ],
            [
                'vAlign' => 'middle',
                'attribute' => 'flag',
                'value' => function($model){
                        return $model->flagText;
                },
                'filter'=>array("0"=>"Non-Active","1"=>"Active"),
                'label'=> "Status",

            ],
        
            [   
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} ', 
                'contentOptions' => ['style' => 'width:14%; white-space: normal;'],
                'buttons' => [
                'delete' => function($url, $model, $key) {     // render your custom button
                    return Html::a('Delete', ['delete', 'id' => $model['id']], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Anda yakin ingin menghapus data ini?',
                            'method' => 'post',
                        ],
                ]);},
                'update' => function($url, $model, $key) {     // render your custom button
                    return Html::a('Edit', ['edit', 'id' => $model['id']], [
                        'class' => 'btn btn-warning',
                ]);},
                'view' => function($url, $model, $key) {     // render your custom button
                    return Html::a('View', ['view', 'id' => $model['id']], [
                        'class' => 'btn btn-info',
                ]);},

                ]

            ],

    
        ];
      
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumns,
            'filterModel' => $searchModel,
            'containerOptions' => ['style' => 'overflow: auto'], 
            'headerRowOptions' => ['class' => 'kartik-sheet-style'],
            'filterRowOptions' => ['class' => 'kartik-sheet-style'],
            'containerOptions' => ['style'=>'overflow: auto'], 
           
            'toolbar' => [
                '{toggleData}',
            ],
            'bordered' => true,
            'striped' => true,
            'condensed' => false,
            'responsive' => true,
            'hover' => true,
            'floatHeader' => true,
            'panel' => [
                'type' => GridView::TYPE_PRIMARY,
                // 'heading' => '<h3 class="panel-title"> Users</h3>',
            ],
    
        ]);
    ?>
   
</div>
  
