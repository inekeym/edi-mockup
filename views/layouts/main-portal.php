<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>



<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<?php if(Yii::$app->user->isGuest){
  echo $this->render(
        'main-login',
        ['content' => $content]
    );
 }else{ ?>
<body>
<?php $this->beginBody() ?>


<div class="wrap" style="">
  <?php
    NavBar::begin([
        // 'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',

        ],
    ]);

    if(Yii::$app->user->identity->job == 3)
    {
        $arrListing = array(
          ['label' => 'Browse Listing', 'url' => ['/listing/browselistingagen']],
          ['label' => 'My Listing', 'url' => ['/listing/indexlistingagen']],
          ['label' => 'Buyer Match', 'url' => ['/buyer-match']],
          ['label' => 'Change Password', 'url' => ['/master-user/changepassword']],
        );
    }

    array_push($arrListing,Yii::$app->user->isGuest ? (
        ['label' => 'Login', 'url' => ['/site/login']]
    ) : (
        '<li>'
        . Html::beginForm(['/site/logout'], 'post')
        . Html::submitButton(
            'Logout (' . Yii::$app->user->identity->username . ')',
            ['class' => 'btn btn-link logout']
        )
        . Html::endForm()
        . '</li>'
    ));

    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $arrListing,
    ]);
    NavBar::end();
  ?>
  <div class="container">
      <div class="row">
          <div class="col-sm-3" ></div>
          <div class="col-sm-6" style="max-width:500px;">
              <!-- <div class="col-md-12" style="border:1px solid black; border-radius: 5px"> -->
                  <?= Alert::widget() ?>
                  <?= $content ?>
                  <br/><br/><br/><br/><br/><br/>

              <!-- </div> -->
          </div>

          <div class="col-sm-3"></div>
      </div>
  </div>
</div>

<!-- </div> -->
<?php $this->endBody() ?>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; EDI Mockup <?= date('Y') ?></p>
    </div>
</footer>

</body>
<?php } ?>
</html>
<?php $this->endPage() ?>
