<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;
use yii\web\YiiAsset;
use yii\widgets\LinkPager;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\grid\GridView;
use app\models\MasterCity;
use yii\helpers\ArrayHelper;

YiiAsset::register($this);

$this->title = "Daftar Pelamar";
?>

<div class="row">
    <div class="col-md-12">
    <h3>Daftar Pelamar</h3>
        <hr/>
       
    </div>
</div>

<div>   
   

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel, 
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],
            [
                'attribute' => 'nama',
                'vAlign' => 'middle',
                'header' =>'Nama',
                'filter' => Html::activeTextInput($searchModel, 'nama', [
                    'class' => 'form-control',
                    'pluginOptions' => [
                        'placeholder'=> '',
                        'allowClear' => true
                    ],
                ]) 
                
            ], 
            [
                'attribute' => 'tempat_lahir',
                'vAlign' => 'middle',
                'header' =>'Tempat & Tanggal Lahir',
                'content' => function($model) {
                    if($model->tempat_lahir){
                      return $model->city->name . ', ' . strtoupper(date('d M Y',strtotime($model->tgl_lahir)));

                    }
                    else{
                      return "-";
                    }
                  },
                  'filter' => \kartik\select2\Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'tempat_lahir',
                    'pluginOptions' => [
                        'placeholder'=> '',
                        'allowClear' => true
                    ],
                    'data' => ArrayHelper::map(MasterCity::find()->all(),'id','name'),
                ]),
                
            ], 
            [
                'attribute' => 'posisi', 
                'vAlign' => 'middle',
                'header' =>'Posisi'
                
            ], 
            [   
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {view} {delete}', 
                'contentOptions' => ['style' => 'width:24%; white-space: normal;'],
                'buttons' => [
                'delete' => function($url, $model, $key) { 
                    return Html::a('Delete <span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $model['id']], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Anda yakin ingin menghapus data ini?',
                            'method' => 'post',
                        ],
                ]);},
                'update' => function($url, $model, $key) {  
                    return Html::a('Edit <span class="glyphicon glyphicon-edit"></span>', ['edit', 'id' => $model['id']], [
                        'class' => 'btn btn-warning',
                ]);},
                'view' => function($url, $model, $key) {   
                    return Html::a('View <span class="glyphicon glyphicon-new-window"></span>', ['view', 'id' => $model['id']], [
                        'class' => 'btn btn-info',
                ]);},

                ]
            ],

        ],
        'options' =>['style' => 'overflow: auto;overflow-x: auto'],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
        ],
        'toolbar' => [
            ['content' =>
                Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], [
                    'class' => 'btn btn-default',
                    'title' => 'Reset Grid',
                    'data-pjax' => 0,
                ]),
            ],
        ],
        ]);
    ?>
    
</div>