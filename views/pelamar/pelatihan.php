<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\file\FileInput;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\Pelamar;
use app\models\MasterCity;
use app\models\PendidikanTerakhir;
use kartik\date\DatePicker;
use unclead\multipleinput\MultipleInput;

$this->title = "Entry Biodata";
?>

<div class="row">
	<div class="col-md-4">
		<h5 style="font-size:15px">RIWAYAT PELATIHAN</h5>
	</div>
</div>
<?= $form->field($model, 'riwayat_pelatihan')->widget(MultipleInput::className(), [
    'max' => 10,
	'options' =>['style' => 'text-transform:uppercase'],
    'columns' => [
        [
          
			'name'  => 'nama_kursus',
            'enableError' => true,
            'title' => 'NAMA KURSUS / SEMINAR',
            'options' => [
                'style' => 'text-transform:uppercase',
                'placeholder' => 'TULISKAN NAMA KURSUS / SEMINAR',
            ]
        ],
		[
            'name'  => 'sertifikat',
            'enableError' => true,
            'title' => 'SERTIFIKAT (ADA/TIDAK)',
            'options' => [
                'style' => 'text-transform:uppercase',
                'placeholder' => 'TULISKAN SERTIFIKAT ADA / TIDAK',
            ]
        ],
        [
            'name'  => 'tahun_awal',
            'type'  => \kartik\date\DatePicker::className(),
            'title' => 'TAHUN MULAI',
            'options' => [
                'pluginOptions' => [
                    'format' => 'yyyy',
                    'todayHighlight' => true,
                    'minViewMode' => 2,
                    'autoclose' => true,

                ],
                'options' => [
                    'placeholder' => 'PILIH TAHUN',
                ]
                
            ],
            'headerOptions' => [
                'style' => 'width: 150px;',
                'placeholder' => 'PILIH TAHUN',

            ]
        ],
        [
            'name'  => 'tahun_akhir',
            'type'  => \kartik\date\DatePicker::className(),
            'title' => 'TAHUN AKHIR',
            'options' => [
                'pluginOptions' => [
                    'format' => 'yyyy',
                    'todayHighlight' => true,
                    'minViewMode' => 2,
                    'autoclose' => true,

                ],
                'options' => [
                    'placeholder' => 'PILIH TAHUN',
                ]
                
            ],
            'headerOptions' => [
                'style' => 'width: 150px;',
                'placeholder' => 'PILIH TAHUN',

            ]
        ],
      
    ],
    'data' => $model->isNewRecord ? [] : $model->pelatihan,

 ])->label(false);
?>
