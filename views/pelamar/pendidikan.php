<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\file\FileInput;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\Pelamar;
use app\models\MasterCity;
use app\models\PendidikanTerakhir;
use kartik\date\DatePicker;
use unclead\multipleinput\MultipleInput;

$this->title = "Entry Biodata";
?>

<div class="row">
	<div class="col-md-4">
		<h5 style="font-size:15px">PENDIDIKAN TERAKHIR</h5>
	</div>
</div>
<?= $form->field($model, 'pendidikan_terakhir')->widget(MultipleInput::className(), [
    'max' => 10,
	'options' =>['style' => 'text-transform:uppercase'],
    'columns' => [
        [
          
			'name'  => 'jenjang',
            'enableError' => true,
			'type'  => kartik\select2\Select2::classname(),
            'title' => 'JENJANG PENDIDIKAN TERAKHIR',
            'options' => [
                'pluginOptions' => ['placeholder' => 'PILIH JENJANG PENDIDIKAN'],
                'data' => Pelamar::selectionPendidikan(),

			],
        ],
		[
            'name'  => 'nama_institusi',
            'enableError' => true,
            'title' => 'NAMA INSTITUSI AKADEMIK',
        
			'options' => [
				'style' => 'text-transform:uppercase',
				'placeholder' => 'TULISKAN NAMA INSTITUSI AKADEMIK',

			]
        ],
		[
            'name'  => 'jurusan',
            'enableError' => true,
			'title' => 'JURUSAN',
			'options' => [
				'style' => 'text-transform:uppercase',
				'placeholder' => 'TULISKAN NAMA JURUSAN',

			]
        ],
        [
            'name'  => 'tahun_lulus',
            'type'  => \kartik\date\DatePicker::className(),
            'title' => 'TAHUN LULUS',
            'options' => [
                'pluginOptions' => [
                    'format' => 'yyyy',
                    'todayHighlight' => true,
					'autoclose' => true,
					'minViewMode' => 2,

				],
                'options' => [
                    'placeholder' => 'PILIH TAHUN',

                ]
            ],
            'headerOptions' => [
                'style' => 'width: 150px;',
            ]
        ],
      
		[
            'name'  => 'ipk',
            'enableError' => true,
			'title' => 'IPK',
			'headerOptions' => [
                'style' => 'width: 80px;',
            ],
			'options' => [
				'style' => 'text-transform:uppercase',
				'placeholder' => 'TULISKAN IPK',

			]
        ],
    ],
	'data' => $model->isNewRecord ? [] : $model->pendidikan,

 ])->label(false);
?>
