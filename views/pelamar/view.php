<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\file\FileInput;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\Pelamar;
use app\models\MasterCity;
use app\models\PendidikanTerakhir;
use kartik\date\DatePicker;
use unclead\multipleinput\MultipleInput;

$this->title = "Entry Biodata";
?>

<?php $form = ActiveForm::begin(); ?>

	<div style="display:flex;justify-content:center">
        <img src="<?= Yii::$app->request->baseUrl ?>/images/logo.png" class=" text-center img-responsive" style="width:30%" >  
    </div>
	<div class="row">
		<div class="col-md-12">
			<h3 class="text-center">DATA PRIBADI PELAMAR</h3>
			<hr/>
		
		</div>
	</div>
	<br>

	<div class="row">
		<div class="col-md-4">
			<h5 style="font-size:15px">POSISI YANG DILAMAR</h5>
		</div>
		<div class="col-md-7">
			<?= $form->field($model, 'posisi')->textInput(['disabled' => true,'maxlength' => true, 'style' => 'text-transform:uppercase','placeholder'=>'TULISKAN POSISI YANG DILAMAR'])->label(false)?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<h5 style="font-size:15px">NAMA LENGKAP</h5>
		</div>
		<div class="col-md-8">
			<?= $form->field($model, 'nama')->textInput(['disabled' => true,'maxlength' => true, 'style' => 'text-transform:uppercase','placeholder'=> 'TULISKAN NAMA LENGKAP ANDA'])->label(false)?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<h5 style="font-size:15px">NO KTP  </h5>
		</div>
		<div class="col-md-8">
			<?= $form->field($model, 'no_ktp')->textInput(['disabled' => true,'type' => 'text','maxLength' => 16, 'placeholder' => 'MASUKAN NOMOR KTP 16 DIGIT'])->label(false)?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<h5 style="font-size:15px">TEMPAT, TANGGAL LAHIR  </h5>
		</div>
		<div class="col-md-4">
			<?= $form->field($model, 'tempat_lahir')->widget(Select2::classname(), [
					'data' => ArrayHelper::map(MasterCity::find()->all(),'id','name'),
					'options' => ['disabled' => true,'placeholder' => 'PILIH TEMPAT LAHIR','maximumSelectionLength' => 3],
					'pluginOptions' => [
						'allowClear' => true,
					],
				])->label(false); ?>
			
		</div>
		<div class="col-md-4">
			<?= 
			$form->field($model, 'tgl_lahir')->widget(DatePicker::classname(), [
				'options' => [
					'placeholder' => 'PILIH TANGGAL LAHIR',
					'disabled' => true,
				],
				'pluginOptions' => [
				'format' => 'dd MM yyyy',
				'autoclose' => true,
				'allowClear' => true,
			]
			])->label(false);
			?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<h5 style="font-size:15px">JENIS KELAMIN</h5>
		</div>
		<div class="col-md-8">
			<?php if($model->jenis_kelamin == 0): ?>
			<?= 'LAKI-LAKI'?>
			<?php else:?>
			<?= 'PEREMPUAN'?>
			<?php endif;?>

		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<h5 style="font-size:15px">AGAMA</h5>
		</div>
		<div class="col-md-8">
			<?=
				$form->field($model, 'agama')->widget(Select2::classname(), [
					'data' => Pelamar::selectionAgama(),
					'options' => ['disabled' => true,'placeholder' => 'PILIH AGAMA'],
					'pluginOptions' => [
						'allowClear' => true,
					],
				])->label(false);
			?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<h5 style="font-size:15px">GOLONGAN DARAH </h5>
		</div>
		<div class="col-md-8">
			<?= 
				$form->field($model, 'gol_dar')->widget(Select2::classname(), [
					'data' => Pelamar::selectionGoldar(),
					'options' => ['disabled' => true,'placeholder' => 'PILIH GOLONGAN DARAH'],
					'pluginOptions' => [
						'allowClear' => true,
					],
				])->label(false);
			?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<h5 style="font-size:15px">STATUS </h5>
		</div>
		<div class="col-md-8">
			<?= 
				$form->field($model, 'status')->widget(Select2::classname(), [
					'data' => Pelamar::selectionStatus(),
					'options' => ['disabled' => true,'placeholder' => 'PILIH STATUS'],
					'pluginOptions' => [
						'allowClear' => true,
					],
				])->label(false);
			?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<h5 style="font-size:15px">ALAMAT KTP</h5>
		</div>
		<div class="col-md-8">
			<?= $form->field($model, 'alamat_ktp')->textArea(['disabled' => true,'rows'=>3,'type' => 'text', 'style' => 'text-transform:uppercase', 'placeholder' => 'TULISKAN ALAMAT KTP'])->label(false)?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<h5 style="font-size:15px">ALAMAT TINGAL</h5>
		</div>
		<div class="col-md-8">
			<?= $form->field($model, 'alamat_skg')->textArea(['disabled' => true,'rows'=>3,'type' => 'text', 'style' => 'text-transform:uppercase', 'placeholder' => 'TULISAN ALAMAT SAAT INI'])->label(false)?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<h5 style="font-size:15px">EMAIL</h5>
		</div>
		<div class="col-md-8">
			<?= $form->field($model, 'email')->textInput(['disabled' => true,'type' => 'email', 'style' => 'text-transform:uppercase', 'placeholder' => 'TULISKAN ALAMAT EMAIL'])->label(false)?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<h5 style="font-size:15px">NO. TELP</h5>
		</div>
		<div class="col-md-8">
			<?= $form->field($model, 'no_tlp')->textInput(['disabled' => true,'type' => 'text','maxLength' => 13, 'placeholder' => 'TULISKAN NO. TELP YANG AKTIF'])->label(false)?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<h5 style="font-size:15px">ORANG TERDEKAT YANG DAPAT DIHUBUNGI</h5>
		</div>
		<div class="col-md-8">
			<?= $form->field($model, 'orang_terdekat')->textInput(['disabled' => true,'maxlength' => true, 'style' => 'text-transform:uppercase','placeholder' => 'TULISKAN ORANG TERDEKAT YANG DAPAT DIHUBUNGI'])->label(false)?>
		</div>
	</div>
	<br>
	<?= $this->render('view_pendidikan', ['model' => $model, 'form' => $form]); ?>
	<br>
	<?= $this->render('view_pelatihan', ['model' => $model, 'form' => $form]); ?>
	<br>
	<?= $this->render('view_pekerjaan', ['model' => $model, 'form' => $form]); ?>
	<div class="row">
		<div class="col-md-4">
			<h5 style="font-size:15px">SKILL</h5>
		</div>
		<div class="col-md-8">
			<?= $form->field($model, 'skill')->textArea(['disabled' => true,'rows'=>3, 'maxlength' => true, 'style' => 'text-transform:uppercase','placeholder' => 'TULISKAN KEAHLIAN & KETERAMPILAN YANG SAAT INI ANDA MILIKI'])->label(false)?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<h5 style="font-size:15px">BERSEDIA DITEMPATKAN DI SELURUH KANTOR PERUSAHAAN</h5>
		</div>
		<div class="col-md-8">
			<?php if($model->bersedia_seluruh_indo == 0): ?>
			<?= 'YA'?>
			<?php else:?>
			<?= 'TIDAK'?>
			<?php endif;?>

		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<h5 style="font-size:15px">PENGHASILAN YANG DIHARAPKAN</h5>
		</div>
		<div class="col-md-6">
			<?= $form->field($model, 'penghasilan_perbulan')->textInput([ 
				'id'=>'gaji_diharapkan', 
				'style' => 'text-transform:uppercase',
				'disabled' => true,
				'placeholder' => 'MASUKAN PENGHASILAN YANG DIHARAPKAN',
				'onkeyup' => 'changeRupiah(this)'
				])->label(false)?>
		</div>
		<div class="col-md-2">
			<h5 style="font-size:15px">/BULAN</h5>
		</div>
	</div>
	<br>
	<br>
	<?php echo Html::a('Back', Yii::$app->request->referrer, ['class' => 'btn btn-default']);?>

	<br>
	<?php
		$script = <<< JS
			$(document).ready(function(){
				$('#pelamar-no_ktp').on('input', function(){
					var value = $(this).val();
					if(value.length > 16){
						$(this).val(value.slice(0, 16));
					}
				});
				$('#pelamar-no_tlp').on('input', function(){
					var value = $(this).val();
					if(value.length > 16){
						$(this).val(value.slice(0, 16));
					}
				});
			});
			
			function changeRupiah(e){
					$(e).val(format($(e).val()));
			}
			var format = function(num){
				var str = num.toString().replace("", ""), parts = false, output = [], i = 1, formatted = null;
				if(str.indexOf(".") > 0) {
					parts = str.split(".");
					str = parts[0];
				}
				str = str.split("").reverse();
				for(var j = 0, len = str.length; j < len; j++) {
					if(str[j] != ",") {
					output.push(str[j]);
					if(i%3 == 0 && j < (len - 1)) {
						output.push(",");
					}
					i++;
					}
				}
			
				formatted = output.reverse().join("");
				return("" + formatted + ((parts) ? "." + parts[1].substr(0, 2) : ""));
			};
			window.changeRupiah = changeRupiah;

		JS;
		$this->registerJs($script);
	?>
<?php ActiveForm::end(); ?>
