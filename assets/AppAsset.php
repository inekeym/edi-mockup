<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/custom.css',
        // 'css/student-portal.css',
        'lightbox/css/visuallightbox.css',
        'lightbox/css/vlightbox2.css',
        'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css',
        'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/brands.min.css',
        'https://cdn.datatables.net/1.13.4/css/jquery.dataTables.min.css',
        // 'https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback',
        // 'css/fontawesome-free/css/all.min.css',
        // 'css/icheck-bootstrap/icheck-bootstrap.min.css',
        // 'css/adminlte.min.css'
    ];
    public $js = [
        "https://malsup.github.io/jquery.form.js",
        "https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js",
        "https:////cdn.datatables.net/1.13.4/js/jquery.dataTables.min.js"
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
