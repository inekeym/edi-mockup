<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\PelamarSearch;
use app\models\Pelamar;
use app\models\PendidikanTerakhir;
use app\models\RiwayatPelatihan;
use app\models\RiwayatPekerjaan;
use yii\data\Pagination;
use yii\web\UploadedFile;
class PelamarController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'as beforeRequest' => [  //if guest user access site so, redirect to login page.
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'actions' => ['login','error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['create','index', 'edit','view','delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $id = Yii::$app->user->getId(); 
        if(Yii::$app->user->identity->getRole() == 1){

            $searchModel = new PelamarSearch();
        
            $dataProvider = $searchModel->search(\Yii::$app->request->get());
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }else {
            $model = Pelamar::find()->where(['created_by' => $id])->orderby(['id'=>SORT_DESC])->one();
            if($model){

                return $this->redirect(['view', 'id' => $model->id]);

            }else{
                return $this->redirect(['create']);

            }

        }
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionCreate()
    {
        $model = new Pelamar();
        $id = Yii::$app->user->getId(); 
        if(Yii::$app->user->identity->getRole() == 0){
            if ($model->load(Yii::$app->request->post())) {
                $transaction = \Yii::$app->db->beginTransaction();
                try{
                    $model->created_by = $id;
               
                    if ($model->save()) {
                    
                        if(isset($_POST['Pelamar']['pendidikan_terakhir'])){

                            foreach($_POST['Pelamar']['pendidikan_terakhir'] as $val){

                                $modelPendidikan = new PendidikanTerakhir();
                                $modelPendidikan->id_pelamar = $model->id;
                                $modelPendidikan->jenjang = $val['jenjang'];
                                $modelPendidikan->nama_institusi = $val['nama_institusi'];
                                $modelPendidikan->jurusan = $val['jurusan'];
                                $modelPendidikan->tahun_lulus = $val['tahun_lulus'];
                                $modelPendidikan->ipk = $val['jenjang'];
                                $modelPendidikan->save();
                              
                            }
                        }

                        if(isset($_POST['Pelamar']['riwayat_pelatihan'])){
                            foreach($_POST['Pelamar']['riwayat_pelatihan'] as $val){

                                $modelPelatihan = new RiwayatPelatihan();
                                $modelPelatihan->id_pelamar = $model->id;
                                $modelPelatihan->nama_kursus = $val['nama_kursus'];
                                $modelPelatihan->sertifikat = $val['sertifikat'];
                                $modelPelatihan->tahun_awal = $val['tahun_awal'];
                                $modelPelatihan->tahun_akhir = $val['tahun_akhir'];
                                $modelPelatihan->save();
                               
                            }
                        }

                        if(isset($_POST['Pelamar']['riwayat_pekerjaan'])){
                            foreach($_POST['Pelamar']['riwayat_pekerjaan'] as $val){
                                $modelPekerjaan = new RiwayatPekerjaan();
                                $modelPekerjaan->id_pelamar = $model->id;
                                $modelPekerjaan->nama_perusahaan = $val['nama_perusahaan'];
                                $modelPekerjaan->posisi = $val['posisi'];
                                $modelPekerjaan->pendapatan_terakhir = $val['pendapatan_terakhir'];
                                $modelPekerjaan->tahun_awal = $val['tahun_awal'];
                                $modelPekerjaan->tahun_akhir = $val['tahun_akhir'];
                                $modelPekerjaan->save();
                                
                            }
                        }
                        $transaction->commit();

                        Yii::$app->session->setFlash('success', 'Data berhasil disimpan');
                        return $this->redirect(['view', 'id' => $model->id]);
                    }else{
                        Yii::$app->session->setFlash('error', 'Data gagal disimpan');
                    }
                    

                }catch (Exception $e) {
                    Yii::error('Error saving model: ' . $e->getMessage()); // Log the error
                    Yii::$app->session->setFlash('error', 'Data gagal disimpan');
                    $transaction->rollBack();
                }
                
           
            } else {
                return $this->render('create', [
                    'model' => $model
                ]);
            }
        }
       
    }


    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionDelete($id)
    {
        $model = Pelamar::findOne($id);

        if ($model !== null) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                // Delete related records
                $model->deleteRelatedRecords();

                // Delete the main record
                $model->delete();

                $transaction->commit();

                Yii::$app->session->setFlash('success', 'Data pelamar berhasil dihapus.');
            } catch (\Exception $e) {
                $transaction->rollBack();
                Yii::$app->session->setFlash('error', 'Data pelamar gagal dihapus : ' . $e->getMessage());
            }
        } else {
            Yii::$app->session->setFlash('error', 'Pelamar not found.');
        }

        return $this->redirect(['index']);
    }

    public function actionEdit($id)
    {
        
        $model = Pelamar::findOne($id);
        $id = Yii::$app->user->getId(); 
        if(Yii::$app->user->identity->getRole() == 1){
            if ($model->load(Yii::$app->request->post())) {
                $transaction = \Yii::$app->db->beginTransaction();
                try{
                    $model->created_by = $id;
               
                    if ($model->save()) {
                        PendidikanTerakhir::deleteAll(['id_pelamar' => $model->id]);
                        RiwayatPelatihan::deleteAll(['id_pelamar' => $model->id]);
                        RiwayatPekerjaan::deleteAll(['id_pelamar' => $model->id]);
        
                        if(isset($_POST['Pelamar']['pendidikan_terakhir'])){

                            foreach($_POST['Pelamar']['pendidikan_terakhir'] as $val){

                                $modelPendidikan = new PendidikanTerakhir();
                                $modelPendidikan->id_pelamar = $model->id;
                                $modelPendidikan->jenjang = $val['jenjang'];
                                $modelPendidikan->nama_institusi = $val['nama_institusi'];
                                $modelPendidikan->jurusan = $val['jurusan'];
                                $modelPendidikan->tahun_lulus = $val['tahun_lulus'];
                                $modelPendidikan->ipk = $val['jenjang'];
                                $modelPendidikan->save();
                                if($modelPendidikan->save()){

                                }else{
                                    var_dump($model->errors);exit;
                                }
                            }
                        }

                        if(isset($_POST['Pelamar']['riwayat_pelatihan'])){
                            foreach($_POST['Pelamar']['riwayat_pelatihan'] as $val){

                                $modelPelatihan = new RiwayatPelatihan();
                                $modelPelatihan->id_pelamar = $model->id;
                                $modelPelatihan->nama_kursus = $val['nama_kursus'];
                                $modelPelatihan->sertifikat = $val['sertifikat'];
                                $modelPelatihan->tahun_awal = $val['tahun_awal'];
                                $modelPelatihan->tahun_akhir = $val['tahun_akhir'];
                                $modelPelatihan->save();
                                if($modelPelatihan->save()){

                                }else{
                                    var_dump($model->errors);exit;
                                }
                            }
                        }

                        if(isset($_POST['Pelamar']['riwayat_pekerjaan'])){
                            foreach($_POST['Pelamar']['riwayat_pekerjaan'] as $val){
                                $modelPekerjaan = new RiwayatPekerjaan();
                                $modelPekerjaan->id_pelamar = $model->id;
                                $modelPekerjaan->nama_perusahaan = $val['nama_perusahaan'];
                                $modelPekerjaan->posisi = $val['posisi'];
                                $modelPekerjaan->pendapatan_terakhir = $val['pendapatan_terakhir'];
                                $modelPekerjaan->tahun_awal = $val['tahun_awal'];
                                $modelPekerjaan->tahun_akhir = $val['tahun_akhir'];
                                $modelPekerjaan->save();
                                if($modelPekerjaan->save()){

                                }else{
                                    var_dump($model->errors);exit;
                                }
                            }
                        }
                        $transaction->commit();

                        Yii::$app->session->setFlash('success', 'Data berhasil disimpan');
                        return $this->redirect(['view', 'id' => $model->id]);
                    }else{
                        Yii::$app->session->setFlash('error', 'Data gagal disimpan');
                        var_dump($model->errors);exit;
                    }
                    

                }catch (Exception $e) {
                    Yii::error('Error saving model: ' . $e->getMessage()); // Log the error
                    Yii::$app->session->setFlash('error', 'Data gagal disimpan');
                    $transaction->rollBack();
                }
            }  else {
                return $this->render('edit', [
                    'model' => $model
                ]);
            }
        }else{
            return $this->redirect(['/site/index']);
        }
       
    }

    public function actionView($id)
    {
        $model = Pelamar::findOne($id);

        if(Yii::$app->user->identity->getRole() == 0){
            // var_dump($model);exit;

            if($model->created_by == Yii::$app->user->getId()){

                // var_dump("asd");exit;
                return $this->render('view', [
                    'model' => $model,
                ]);
            }
        }else{
            // var_dump("tes");exit;

            return $this->render('view', [
                'model' => $model,
            ]);
        }
    }

   
}
