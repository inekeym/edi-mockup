<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\MasterUser;
use app\models\UserSearch;
use app\models\AuditLog;
use yii\data\Pagination;

class UserController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        
        $searchModel = new UserSearch();
    
        $dataProvider = $searchModel->search(\Yii::$app->request->get());
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionCreate()
    {
        $model = new MasterUser();

        if (Yii::$app->request->post()) {
            $model->load(Yii::$app->request->post());
            $pass = $model->password;
            $model->password = Yii::$app->getSecurity()->generatePasswordHash($pass);
            
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Data berhasil disimpan');
            } else {
                Yii::$app->session->setFlash('error', 'Data gagal disimpan');
            }
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionVisible($id)
    {
        $model = MasterUser::findOne($id);

        $model->load(Yii::$app->request->post());
        $model->flag = 1;
        if ($model->save()) {                
                Yii::$app->session->setFlash('success', 'Data berhasil ditampilkan');
        } else {
                Yii::$app->session->setFlash('error', 'Data gagal ditampilkan');
        }
        return $this->redirect(['index']);
    }
    public function actionHide($id)
    {
        $model = MasterUser::findOne($id);
        $model->load(Yii::$app->request->post());
        $model->flag = 0;
        if ($model->save()) {                
                Yii::$app->session->setFlash('success', 'Data berhasil disembunyikan');
        } else {
                Yii::$app->session->setFlash('error', 'Data gagal disembunyikan');
        }
        return $this->redirect(['index']);
    }
    
    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionDelete($id)
    {
        $model = MasterUser::findOne($id);

            $model->flag = 0; 
          
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Data berhasil dihapus');

                return $this->redirect(['index']);
            }

    }

    public function actionEdit($id)
    {
        $model = MasterUser::findOne($id);
        $password_lama = $model->password; // backup the value first;
        $model->password = "";

        if (Yii::$app->request->post()) {
            $check = $model->load(Yii::$app->request->post());
            $password_baru = $model->password;

            if(strlen($password_baru) == 0){
                $model->password = $password_lama;
            } else{
                $model->password = Yii::$app->getSecurity()->generatePasswordHash($password_baru);
            }
            

            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Data berhasil disimpan');
            } else {
                Yii::$app->session->setFlash('error', 'Data gagal disimpan');
            }
            return $this->redirect(['index']);
        } else {
            return $this->render('edit', [
                'model' => $model,
            ]);
        }
    }

 

   
}
